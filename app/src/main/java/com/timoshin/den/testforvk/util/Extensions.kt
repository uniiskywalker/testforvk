package com.timoshin.den.testforvk.util

import android.content.res.Resources
import android.util.DisplayMetrics
import android.view.View

val String.Companion.EMPTY: String
    get() = ""

fun View.convertDpToPixel(dp: Float): Float {
    return dp * (Resources.getSystem().displayMetrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)
}

data class Quadruple<out A, out B, out C, out D>(
        val first: A,
        val second: B,
        val third: C,
        val fourth: D
) {

    override fun toString(): String = "($first, $second, $third, $fourth)"
}

fun <A, B, C, D> quadruple(
        first: A, second: B,
        third: C,
        fourth: D
): Quadruple<A, B, C, D> = Quadruple(first, second, third, fourth)

