package com.timoshin.den.testforvk.editor.presentation.ui.editor

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Color
import android.support.constraint.ConstraintLayout
import android.support.v4.content.ContextCompat
import android.support.v4.widget.TextViewCompat
import android.text.Editable
import android.text.SpannableString
import android.text.Spanned
import android.text.TextWatcher
import android.util.AttributeSet
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import com.timoshin.den.testforvk.R
import com.timoshin.den.testforvk.editor.presentation.ui.editor.EditorViewLayout.STYLES.STYLE_SIMPLE
import com.timoshin.den.testforvk.editor.presentation.ui.editor.EditorViewLayout.STYLES.STYLE_WRAP_TEXT
import com.timoshin.den.testforvk.editor.presentation.ui.editor.EditorViewLayout.STYLES.STYLE_WRAP_TEXT_WITH_ALPHA
import java.util.*

class EditorViewLayout : ConstraintLayout {

    private val mEditText: EditText
    private val mEditorView: EditorView
    private val mTextBackgroundView: TextBackgroundView
    private val mInputMethodManager: InputMethodManager

    var style = STYLE_SIMPLE
        set(value) {
            field = value
            val (text, textAppearance) = applyStyle(value, mEditText.text)
            TextViewCompat.setTextAppearance(mEditText, textAppearance)
            mTextBackgroundView.clearLines()
            val currentSelectionStart = mEditText.selectionStart
            val currentSelectionEnd = mEditText.selectionEnd
            mEditText.setText(text)
            mEditText.setSelection(currentSelectionStart, currentSelectionEnd)
        }

    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)

    init {
        isDrawingCacheEnabled = true
        inflate(context, R.layout.layout_editor, this)
        mEditorView = findViewById(R.id.ev_surface)
        mEditText = findViewById(R.id.tv_text)
        mTextBackgroundView = findViewById(R.id.text_background)

        mInputMethodManager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        mEditorView.setOnClickHandler {
            if (mEditText.hasFocus()) {
                mEditText.clearFocus()
                mInputMethodManager.hideSoftInputFromWindow(mEditText.windowToken, 0)
            } else {
                mEditText.requestFocus().takeIf { it }?.apply {
                    mEditText.setSelection(mEditText.text.length)
                    mInputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
                }
            }
        }
        mEditText.addTextChangedListener(
                object : TextWatcherAdapter() {
                    override fun afterTextChanged(s: Editable?) {
                        if (s == null) return
                        mEditText.removeTextChangedListener(this)
                        mTextBackgroundView.clearLines()
                        val (text, textAppearance) = applyStyle(style, s)
                        TextViewCompat.setTextAppearance(mEditText, textAppearance)
                        s.replace(0, s.length, text)
                        mEditText.addTextChangedListener(this)
                    }
                }
        )
    }

    fun setBackgroundImage(background: Bitmap, top: Bitmap?, bottom: Bitmap?, scaleType: Int) =
            mEditorView.setBackgroundImage(background, top, bottom, scaleType)

    fun addSticker(bitmap: Bitmap) = mEditorView.addSticker(bitmap)

    private fun applyStyle(style: Int, text: CharSequence): Pair<CharSequence, Int> = when (style) {
        STYLE_WRAP_TEXT -> applySpannable(text, Color.WHITE, R.style.TextAppearance_VkRecordTextStyle)
        STYLE_WRAP_TEXT_WITH_ALPHA -> applySpannable(
                text, ContextCompat.getColor(context, R.color.semi_white),
                R.style.TextAppearance_VkRecordTextStyle_Inverse
        )
        else -> applySpannable(text, Color.TRANSPARENT, R.style.TextAppearance_VkRecordTextStyle)
    }

    private fun applySpannable(text: CharSequence, color: Int, textAppearance: Int): Pair<CharSequence, Int> {
        mTextBackgroundView.setColor(color)
        return SpannableString.valueOf(text).apply {
            setSpan(
                    PaddingBackgroundColorSpan()
                    { lineNumber: Int, start: Int, end: Int, top: Int, bottom: Int ->
                        mTextBackgroundView.addLine(lineNumber, start, end, top, bottom)
                    },
                    0,
                    text.length,
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )
        } to textAppearance
    }

    object STYLES {
        const val STYLE_SIMPLE = 0
        const val STYLE_WRAP_TEXT = 1
        const val STYLE_WRAP_TEXT_WITH_ALPHA = 2
    }

    fun getFinalImage(): Bitmap {
        val currentBitmap: Bitmap
        if (mEditText.text.isEmpty()) {
            mEditText.visibility = View.INVISIBLE
            currentBitmap = drawingCache
            mEditText.visibility = View.VISIBLE
        } else {
            currentBitmap = drawingCache
        }
        return if(mEditorView.fieldWidth.toInt() != 0 &&  mEditorView.fieldHeight.toInt() != 0){
            Bitmap.createBitmap(
                    currentBitmap,
                    mEditorView.fieldX.toInt(),
                    mEditorView.fieldY.toInt(),
                    mEditorView.fieldWidth.toInt(),
                    mEditorView.fieldHeight.toInt()
            )
        } else {
            currentBitmap
        }
    }

    fun getComplicatedSavedState(): Any {
        return mEditorView.mStickers
    }

    @Suppress("UNCHECKED_CAST")
    fun restoreComplicatedSavedState(any: Any?) {
        if (any != null && any is Deque<*>) {
            mEditorView.mStickers = any as Deque<EditorView.Sticker>
            mEditorView.postInvalidate()
        }
    }
}

abstract class TextWatcherAdapter : TextWatcher {
    override fun afterTextChanged(s: Editable?) {}

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
}