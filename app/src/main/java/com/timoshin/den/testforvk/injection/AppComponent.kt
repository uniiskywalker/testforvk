package com.timoshin.den.testforvk.injection

import com.timoshin.den.testforvk.App
import com.timoshin.den.testforvk.injection.mainactivity.MainActivityProvider
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule


@PerApp
@Component(
        modules = [
            AndroidSupportInjectionModule::class,
            MainActivityProvider::class
        ]
)
interface AppComponent : AndroidInjector<App> {

    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<App>()

}