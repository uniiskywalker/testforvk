package com.timoshin.den.testforvk.editor.domain.entity

import android.graphics.Bitmap
import android.graphics.drawable.Drawable

data class BackgroundItem(
        val id: Int,
        val thumb: Drawable,
        val background: Bitmap,
        var top: Bitmap? = null,
        var bottom: Bitmap? = null,
        val scale: Int = ScaleType.NONE
)

object ScaleType {
    const val NONE = 0
    const val STRETCH = 1
}
