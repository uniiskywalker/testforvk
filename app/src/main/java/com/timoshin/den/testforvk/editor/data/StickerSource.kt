package com.timoshin.den.testforvk.editor.data

import android.content.Context
import com.timoshin.den.testforvk.editor.domain.entity.StickerItem
import com.timoshin.den.testforvk.editor.domain.interfaces.Reader
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class StickerSource(ctx: Context) : FromAssetsSource(ctx), Reader<StickerItem> {

    private val pathTemplate = "stickers/%d.png"

    override fun getAll(): Flowable<StickerItem> {
        return Flowable.range(1, 24)
                .flatMapSingle { id ->
                    Single.just(getAsset(pathTemplate.format(id))).map { StickerItem(id, it) }
                            .subscribeOn(Schedulers.io())
                }.observeOn(AndroidSchedulers.mainThread())
    }
}