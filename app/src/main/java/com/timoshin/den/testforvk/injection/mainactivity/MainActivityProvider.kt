package com.timoshin.den.testforvk.injection.mainactivity

import com.timoshin.den.testforvk.editor.presentation.ui.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
interface MainActivityProvider {

    @PerMainActivity
    @ContributesAndroidInjector(modules = [
        MainActivityModule::class,
        MainActivityBinds::class
    ])
    fun mainActivityInjector(): MainActivity
}