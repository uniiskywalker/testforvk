package com.timoshin.den.testforvk.editor.domain.interfaces

import io.reactivex.Flowable

interface Reader<T> {

    fun getAll(): Flowable<T>
}
