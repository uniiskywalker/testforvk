package com.timoshin.den.testforvk.editor.presentation.ui

import android.graphics.Bitmap
import com.timoshin.den.testforvk.editor.presentation.base.MvpPresenter
import com.timoshin.den.testforvk.editor.presentation.base.MvpView

interface MainActivityContract {

    interface View : MvpView {

        fun setImage(
                background: Bitmap,
                top: Bitmap?,
                bottom: Bitmap?,
                scaleType: Int
        )

        fun callGallery()

        fun addSticker(stickerBitmap: Bitmap)

        fun hideStickerMenu()

        fun showOkSaving()
    }

    interface Presenter : MvpPresenter<View> {

        fun addNewImage()

        fun addDefaultImage(
                background: Bitmap,
                top: Bitmap?,
                bottom: Bitmap?,
                scaleType: Int
        )

        fun onStickerSelected(id: Int, stickerBitmap: Bitmap)

        fun onSave(finalImage: Bitmap)

        fun startLoading(onFinishLoad: () -> Unit)
    }
}