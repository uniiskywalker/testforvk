package com.timoshin.den.testforvk.editor.presentation.base

interface MvpAdapter<T> {

    fun <P : MvpPresenter<out MvpView>> attachPresenter(presenter: P)

    fun addList(item: List<T>)

    operator fun get(position: Int): T
}