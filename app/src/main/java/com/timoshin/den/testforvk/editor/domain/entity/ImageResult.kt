package com.timoshin.den.testforvk.editor.domain.entity

import android.graphics.Bitmap

data class ImageResult(val bitmap: Bitmap)