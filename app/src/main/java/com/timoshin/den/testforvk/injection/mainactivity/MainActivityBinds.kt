package com.timoshin.den.testforvk.injection.mainactivity

import com.timoshin.den.testforvk.editor.data.BackgroundItemSource
import com.timoshin.den.testforvk.editor.data.ImageWriter
import com.timoshin.den.testforvk.editor.data.StickerSource
import com.timoshin.den.testforvk.editor.domain.entity.BackgroundItem
import com.timoshin.den.testforvk.editor.domain.entity.ImageResult
import com.timoshin.den.testforvk.editor.domain.entity.StickerItem
import com.timoshin.den.testforvk.editor.domain.interfaces.Reader
import com.timoshin.den.testforvk.editor.domain.interfaces.Writer
import com.timoshin.den.testforvk.editor.presentation.base.MvpAdapter
import com.timoshin.den.testforvk.editor.presentation.ui.MainActivityContract
import com.timoshin.den.testforvk.editor.presentation.ui.MainActivityPresenter
import com.timoshin.den.testforvk.editor.presentation.ui.adapter.BackgroundsAdapter
import com.timoshin.den.testforvk.editor.presentation.ui.adapter.StickersAdapter
import dagger.Binds
import dagger.Module

@Module
interface MainActivityBinds {

    @Binds
    fun bindMainActivityPresenter(presenter: MainActivityPresenter): MainActivityContract.Presenter

    @Binds
    fun bindBackgroundsAdapter(adapter: BackgroundsAdapter): MvpAdapter<BackgroundItem>

    @Binds
    fun bindStickersAdapter(adapter: StickersAdapter): MvpAdapter<StickerItem>

    @Binds
    fun bindBackgroundItemSource(source: BackgroundItemSource): Reader<BackgroundItem>

    @Binds
    fun bindStickerSource(source: StickerSource): Reader<StickerItem>

    @Binds
    fun bindImageWriter(imageWriter: ImageWriter): Writer<ImageResult>
}