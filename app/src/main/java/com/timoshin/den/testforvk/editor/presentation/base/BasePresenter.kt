package com.timoshin.den.testforvk.editor.presentation.base

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable


abstract class BasePresenter<T : MvpView> : MvpPresenter<T> {

    var view: T? = null
        private set

    private val mViewDependingCompositeDisposables = CompositeDisposable()

    private val mPayload = HashMap<String, Any>()

    val isViewAttached: Boolean
        get() = view != null

    override fun prepare() {

    }

    override fun destroy() {

    }

    override fun attachView(mvpView: T) {
        view = mvpView
    }

    override fun detachView() {
        mViewDependingCompositeDisposables.clear()
        view = null
    }

    protected fun addDisposable(disposable: Disposable) {
        this.mViewDependingCompositeDisposables.add(disposable)
    }

    override fun deliverPayload(payload: Map<String, Any>) {
        this.mPayload.putAll(payload)
    }

    fun <P> getPayloadByKey(key: String, pClass: Class<P>): P? {
        return pClass.cast(mPayload[key])
    }
}
