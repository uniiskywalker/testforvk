package com.timoshin.den.testforvk.editor.presentation.ui.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.timoshin.den.testforvk.R
import com.timoshin.den.testforvk.editor.domain.entity.StickerItem
import com.timoshin.den.testforvk.editor.presentation.base.MvpAdapter
import com.timoshin.den.testforvk.editor.presentation.base.MvpPresenter
import com.timoshin.den.testforvk.editor.presentation.base.MvpView
import com.timoshin.den.testforvk.editor.presentation.ui.MainActivityContract

class StickersAdapter : RecyclerView.Adapter<StickersAdapter.ItemViewHolder>(), MvpAdapter<StickerItem> {

    private lateinit var mPresenter: MainActivityContract.Presenter
    private lateinit var mStickerItems: List<StickerItem>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        return ItemViewHolder(inflate(parent, R.layout.sticker_item) as ImageView)
    }

    private fun inflate(parent: ViewGroup, res: Int): View? {
        return LayoutInflater.from(parent.context).inflate(res, parent, false)
    }

    override fun getItemCount(): Int = mStickerItems.size

    override fun onBindViewHolder(holderItem: ItemViewHolder, position: Int) {
        holderItem.ib.setImageBitmap(mStickerItems[position].stickerBitmap)
        holderItem.ib.setOnClickListener {
            mPresenter.onStickerSelected(mStickerItems[position].id, mStickerItems[position].stickerBitmap)
        }
    }

    class ItemViewHolder(val ib: ImageView) : RecyclerView.ViewHolder(ib)

    override fun <P : MvpPresenter<out MvpView>> attachPresenter(presenter: P) {
        mPresenter = presenter as MainActivityContract.Presenter
    }

    override fun addList(item: List<StickerItem>) {
        mStickerItems = item
    }

    override fun get(position: Int): StickerItem = mStickerItems[position]
}