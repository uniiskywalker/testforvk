package com.timoshin.den.testforvk.editor.presentation.ui.editor

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.MotionEvent
import android.widget.ImageView
import com.timoshin.den.testforvk.R
import java.util.*


class EditorView : ImageView {

    var fieldX: Float = 0f
        private set
    var fieldY: Float = 0f
        private set
    var fieldHeight: Float = 0f
        private set
    var fieldWidth: Float = 0f
        private set

    private var mFieldCX: Float = 0f
    private var mFieldCY: Float = 0f

    internal var mStickers: Deque<Sticker> = ArrayDeque<Sticker>()

    private val mClipOutRect: RectF = RectF()

    private val mGestureHandler: GestureHandler<Sticker>

    private lateinit var mTrashClosedBitmap: Bitmap
    private lateinit var mTrashOpenedBitmap: Bitmap
    private var mCurrentlyShowedTrash: Bitmap? = null

    private var onClickHandler: (() -> Unit)? = null

    constructor(context: Context?) : super(context) {
        context?.apply { init(this, null) }
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        context?.apply { init(this, attrs) }
    }

    private fun init(context: Context, attrs: AttributeSet?) {
        val arr = context.obtainStyledAttributes(attrs, R.styleable.EditorView)
        val trashClosedRes = arr.getResourceId(R.styleable.EditorView_trash_closed, 0)
        val trashOpenedRes = arr.getResourceId(R.styleable.EditorView_trash_opened, 0)
        if (trashClosedRes != 0) mTrashClosedBitmap = BitmapFactory.decodeResource(context.resources, trashClosedRes)
        if (trashOpenedRes != 0) mTrashOpenedBitmap = BitmapFactory.decodeResource(context.resources, trashOpenedRes)
        arr.recycle()
    }

    init {
        mGestureHandler = GestureHandler(
                context,
                { targetX: Float, targetY -> retrieveSticker(targetX, targetY) },

                { obj: Sticker, prevX: Float, prevY: Float, newX: Float, newY: Float ->
                    translateSticker(obj, prevX, prevY, newX, newY)
                },

                { obj: Sticker -> watchStickerRemoving(obj) },

                { obj: Sticker, scaleDiff: Float -> scaleSticker(obj, scaleDiff) },

                { obj: Sticker, angle: Float, relativePointX: Float, relativePointY: Float ->
                    rotateSticker(obj, angle, relativePointX, relativePointY)
                }
        )
    }

    fun setBackgroundImage(background: Bitmap, top: Bitmap?, bottom: Bitmap?, scaleType: Int) =
            setUpBackground(background, top, bottom, scaleType)

    private fun setUpBackground(background: Bitmap,
                                top: Bitmap?,
                                bottom: Bitmap?,
                                scaleType: Int) {
        var base = Bitmap.createBitmap(measuredWidth, measuredHeight, background.config)
        val canvas = Canvas(base)
        val matrix = Matrix()

        when (scaleType) {
            ScaleType.STRETCH -> {
                matrix.reset()
                canvas.apply {
                    drawBitmap(background, matrix.apply {
                        setScale(
                                measuredWidth.toFloat() / background.width,
                                measuredHeight.toFloat() / background.height
                        )
                    }, null)
                }
            }
            else -> base = background
        }

        if (top != null && bottom != null) {
            canvas.apply {
                matrix.reset()
                drawBitmap(top, matrix.apply {
                    setScale(
                            measuredWidth.toFloat() / top.width,
                            measuredWidth.toFloat() / top.width
                    )
                }, null)
                matrix.reset()
                drawBitmap(bottom, matrix.apply {
                    val scaledWidth = measuredWidth.toFloat() / bottom.width
                    val scaledHeight = measuredWidth.toFloat() / bottom.width
                    setScale(scaledWidth, scaledHeight)
                    postTranslate(0f, measuredHeight - bottom.height * scaledHeight)
                }, null)
            }
        }
        this.setImageBitmap(base)
    }

    fun setOnClickHandler(handler: (() -> Unit)?) {
        this.onClickHandler = handler
    }

    fun addSticker(stickerBitmap: Bitmap) {
        if (fieldWidth == 0f && fieldHeight == 0f) return
        val mtx = Matrix()
        val x: Float = (mFieldCX - stickerBitmap.width / 2)
        val y: Float = (mFieldCY - stickerBitmap.height / 2)
        mtx.setTranslate(x, y)
        val rect = RectF(0f, 0f, stickerBitmap.width.toFloat(), stickerBitmap.height.toFloat())
        mtx.mapRect(rect)
        mStickers.offerFirst(Sticker(stickerBitmap, mtx, rect))
        invalidate()
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        if (drawable == null) return

        val ratio: Float = Math.min(
                measuredWidth.toFloat() / drawable.intrinsicWidth, measuredHeight.toFloat() / drawable.intrinsicHeight
        )
        fieldWidth = drawable.intrinsicWidth * ratio
        fieldHeight = drawable.intrinsicHeight * ratio
        fieldX = measuredWidth / 2 - fieldWidth / 2
        fieldY = measuredHeight / 2 - fieldHeight / 2
        mFieldCX = fieldX + fieldWidth / 2
        mFieldCY = fieldY + fieldHeight / 2

        mClipOutRect.set(fieldX, fieldY, fieldX + fieldWidth, fieldY + fieldHeight)
    }

    private var moveCount: Int = 0

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        if (event == null) return false
        if (event.actionMasked == MotionEvent.ACTION_MOVE) moveCount++
        else if (event.actionMasked == MotionEvent.ACTION_UP) {
            if (moveCount < 5) onClickHandler?.let { it() }
            moveCount = 0
        }
        return mGestureHandler.handleTouchEvent(event)
    }

    private fun retrieveSticker(touchX: Float, touchY: Float): Sticker? {
        var searchedSt: Sticker? = null
        a@ for (st in mStickers) {
            if (st.rect.contains(touchX, touchY)) {
                searchedSt = st
                break@a
            }
        }
        if (searchedSt != null) {
            mStickers.remove(searchedSt)
            mStickers.offerFirst(searchedSt)
        }
        return searchedSt
    }

    private fun translateSticker(st: Sticker, prevX: Float, prevY: Float, newX: Float, newY: Float) =
            st.apply {
                matrix.postTranslate(newX - prevX, newY - prevY)
                resetStickerState(this)
                mCurrentlyShowedTrash = if (isStickerOutOfField(this)) {
                    mTrashOpenedBitmap
                } else {
                    mTrashClosedBitmap
                }
                invalidate()
            }

    private fun scaleSticker(st: Sticker, scaleDiff: Float) = st.apply {
        if ((rect.right - rect.left < fieldWidth / 8 && scaleDiff < 1) ||
                (rect.right - rect.left > fieldWidth * 1.2 && scaleDiff > 1)
        ) return@apply
        val rectCX = rect.left + (rect.right - rect.left) / 2
        val rectCY = rect.top + (rect.bottom - rect.top) / 2
        matrix.postScale(scaleDiff, scaleDiff, rectCX, rectCY)
        resetStickerState(this)
        invalidate(rect.left.toInt(), rect.top.toInt(), rect.right.toInt(), rect.bottom.toInt())
    }

    private fun rotateSticker(st: Sticker, angle: Float, x: Float, y: Float) = st.apply {
        matrix.postRotate(angle, x, y)
        resetStickerState(this)
        invalidate(rect.left.toInt(), rect.top.toInt(), rect.right.toInt(), rect.bottom.toInt())
    }

    private fun watchStickerRemoving(st: Sticker) {
        if (isStickerOutOfField(st)) mStickers.remove(st)
        mCurrentlyShowedTrash = null
        invalidate()
    }

    private fun isStickerOutOfField(st: Sticker): Boolean {
        val rectCX = st.rect.left + (st.rect.right - st.rect.left) / 2
        val rectCY = st.rect.top + (st.rect.bottom - st.rect.top) / 2
        return (rectCX < fieldX ||
                rectCY < fieldY ||
                rectCX > fieldX + fieldWidth ||
                rectCY > fieldY + fieldHeight)
    }

    private fun resetStickerState(st: Sticker) {
        st.rect.set(0f, 0f, st.bitmap.width.toFloat(), st.bitmap.height.toFloat())
        st.matrix.mapRect(st.rect)
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        canvas?.apply {
            clipRect(mClipOutRect)
            mStickers.descendingIterator().forEach { drawBitmap(it.bitmap, it.matrix, null) }
            mCurrentlyShowedTrash?.apply {
                drawBitmap(
                        this,
                        mFieldCX - width / 2,
                        fieldY + fieldHeight - height * 1.5f,
                        null
                )
            }
        }
    }

    data class Sticker(val bitmap: Bitmap, val matrix: Matrix, val rect: RectF)

    object ScaleType {
        const val NONE = 0
        const val STRETCH = 1
    }

}