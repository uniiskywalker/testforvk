package com.timoshin.den.testforvk.editor.domain.interfaces

import io.reactivex.Completable

interface Writer<V> {

    fun create(value: V): Completable
}