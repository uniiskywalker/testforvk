package com.timoshin.den.testforvk.injection.mainactivity

import com.timoshin.den.testforvk.editor.data.BackgroundItemSource
import com.timoshin.den.testforvk.editor.data.ImageWriter
import com.timoshin.den.testforvk.editor.data.StickerSource
import com.timoshin.den.testforvk.editor.domain.entity.BackgroundItem
import com.timoshin.den.testforvk.editor.domain.entity.ImageResult
import com.timoshin.den.testforvk.editor.domain.entity.StickerItem
import com.timoshin.den.testforvk.editor.domain.interactors.EditorInteractor
import com.timoshin.den.testforvk.editor.domain.interfaces.Reader
import com.timoshin.den.testforvk.editor.domain.interfaces.Writer
import com.timoshin.den.testforvk.editor.presentation.ui.ImageLoader
import com.timoshin.den.testforvk.editor.presentation.ui.MainActivity
import com.timoshin.den.testforvk.editor.presentation.ui.adapter.BackgroundsAdapter
import com.timoshin.den.testforvk.editor.presentation.ui.adapter.StickersAdapter
import dagger.Module
import dagger.Provides

@Module
class MainActivityModule {

    @Provides
    @PerMainActivity
    fun provideBackgroundsAdapter(context: MainActivity) = BackgroundsAdapter(context)

    @Provides
    @PerMainActivity
    fun provideBackgroundItemSource(context: MainActivity) = BackgroundItemSource(context)

    @Provides
    @PerMainActivity
    fun provideEditorInteractor(
            reader1: Reader<BackgroundItem>,
            reader2: Reader<StickerItem>,
            writer: Writer<ImageResult>
    ) = EditorInteractor(reader1, reader2, writer)

    @Provides
    @PerMainActivity
    fun provideStickerAdapter() = StickersAdapter()

    @Provides
    @PerMainActivity
    fun provideStickerSource(context: MainActivity) = StickerSource(context)

    @Provides
    @PerMainActivity
    fun provideImageWriter(context: MainActivity) = ImageWriter(context)

    @Provides
    @PerMainActivity
    fun provideImageLoader(context: MainActivity) = ImageLoader(context)
}