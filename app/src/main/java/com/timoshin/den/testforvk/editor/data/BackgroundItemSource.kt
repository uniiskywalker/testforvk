package com.timoshin.den.testforvk.editor.data

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.support.v4.content.ContextCompat
import com.timoshin.den.testforvk.R
import com.timoshin.den.testforvk.editor.domain.entity.BackgroundItem
import com.timoshin.den.testforvk.editor.domain.entity.ScaleType
import com.timoshin.den.testforvk.editor.domain.interfaces.Reader
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class BackgroundItemSource(ctx: Context) : FromAssetsSource(ctx), Reader<BackgroundItem> {

    override fun getAll(): Flowable<BackgroundItem> = Flowable.fromIterable(getMockList())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())

    private fun getMockList(): List<BackgroundItem> {
        return listOf(
                BackgroundItem(
                        0,
                        ContextCompat.getDrawable(context, R.drawable.stub_background_thumb)!!,
                        getBitmap(R.drawable.stub_background)
                ),
                BackgroundItem(
                        1,
                        ContextCompat.getDrawable(context, R.drawable.orange_background_thumb)!!,
                        getBitmap(R.drawable.orange_background)
                ),
                BackgroundItem(
                        2,
                        ContextCompat.getDrawable(context, R.drawable.thumb_beach)!!,
                        getAsset("illustrations/beach/bg_beach_center.png"),
                        getAsset("illustrations/beach/bg_beach_top.png"),
                        getAsset("illustrations/beach/bg_beach_bottom.png"),
                        ScaleType.STRETCH
                ),
                BackgroundItem(
                        3,
                        ContextCompat.getDrawable(context, R.drawable.thumb_stars)!!,
                        getAsset("illustrations/stars/bg_stars_center.png")
                )
        )
    }

    private fun getBitmap(drawableRes: Int): Bitmap {
        val drawable = ContextCompat.getDrawable(context, drawableRes)!!
        val canvas = Canvas()
        val bitmap = Bitmap.createBitmap(drawable.intrinsicWidth, drawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
        canvas.setBitmap(bitmap)
        drawable.setBounds(0, 0, drawable.intrinsicWidth, drawable.intrinsicHeight)
        drawable.draw(canvas)

        return bitmap
    }
}