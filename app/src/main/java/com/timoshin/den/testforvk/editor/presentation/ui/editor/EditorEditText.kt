package com.timoshin.den.testforvk.editor.presentation.ui.editor

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import android.widget.EditText

class EditorEditText : EditText {

    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        return if (isFocused) {
            super.onTouchEvent(event)
        } else {
            false
        }
    }
}