package com.timoshin.den.testforvk.editor.domain.interactors

import com.timoshin.den.testforvk.editor.domain.entity.BackgroundItem
import com.timoshin.den.testforvk.editor.domain.entity.ImageResult
import com.timoshin.den.testforvk.editor.domain.entity.StickerItem
import com.timoshin.den.testforvk.editor.domain.interfaces.Reader
import com.timoshin.den.testforvk.editor.domain.interfaces.Writer
import io.reactivex.Single
import io.reactivex.functions.BiFunction

class EditorInteractor(
        private val mBgItemReader: Reader<BackgroundItem>,
        private val mStickerSource: Reader<StickerItem>,
        private val mImageWriter: Writer<ImageResult>
) {

    fun getResources(): Single<Pair<List<BackgroundItem>, List<StickerItem>>> = mBgItemReader.getAll()
            .toList()
            .zipWith(mStickerSource.getAll()
                    .toList(),
                    BiFunction<List<BackgroundItem>,
                            List<StickerItem>,
                            Pair<List<BackgroundItem>, List<StickerItem>>>
                    { bgs, stickers -> return@BiFunction bgs to stickers })

    fun saveImage(result: ImageResult) = mImageWriter.create(result)
}