package com.timoshin.den.testforvk.editor.presentation.ui

import com.timoshin.den.testforvk.injection.PerApp
import javax.inject.Inject

@PerApp
class StateHolder @Inject constructor() {
    private val map = HashMap<String, Any>()

    fun saveState(key: String, value: Any) {
        map[key] = value
    }

    fun getState(key: String): Any? {
        val value = map[key]
        map.remove(key)
        return value
    }
}