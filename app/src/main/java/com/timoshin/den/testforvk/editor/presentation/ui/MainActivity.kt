package com.timoshin.den.testforvk.editor.presentation.ui

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.BottomSheetDialog
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.LinearLayoutManager.HORIZONTAL
import android.support.v7.widget.RecyclerView
import android.view.ViewTreeObserver
import android.widget.Button
import android.widget.ImageButton
import android.widget.Toast
import com.timoshin.den.testforvk.R
import com.timoshin.den.testforvk.editor.presentation.ui.adapter.BackgroundsAdapter
import com.timoshin.den.testforvk.editor.presentation.ui.adapter.StickersAdapter
import com.timoshin.den.testforvk.editor.presentation.ui.editor.EditorView
import com.timoshin.den.testforvk.editor.presentation.ui.editor.EditorViewLayout
import com.timoshin.den.testforvk.editor.presentation.ui.editor.EditorViewLayout.STYLES.STYLE_SIMPLE
import com.timoshin.den.testforvk.editor.presentation.ui.editor.EditorViewLayout.STYLES.STYLE_WRAP_TEXT
import com.timoshin.den.testforvk.editor.presentation.ui.editor.EditorViewLayout.STYLES.STYLE_WRAP_TEXT_WITH_ALPHA
import dagger.android.AndroidInjection
import javax.inject.Inject


class MainActivity : AppCompatActivity(), MainActivityContract.View {

    @Inject
    lateinit var mPresenter: MainActivityContract.Presenter

    @Inject
    lateinit var mBgAdapter: BackgroundsAdapter

    @Inject
    lateinit var mStickerAdapter: StickersAdapter

    @Inject
    lateinit var mStateHolder: StateHolder

    @Inject
    lateinit var mImageLoader: ImageLoader

    private lateinit var mRvBackgrounds: RecyclerView
    private lateinit var mEditorViewLayout: EditorViewLayout
    private lateinit var mBottomSheetDialog: BottomSheetDialog

    private var mUserIsSelectingPic = false

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mEditorViewLayout = findViewById(R.id.evl_surface)
        mRvBackgrounds = findViewById(R.id.rv_backgrounds)
        mRvBackgrounds.apply {
            layoutManager = LinearLayoutManager(this@MainActivity)
                    .apply { orientation = HORIZONTAL }
            adapter = mBgAdapter
        }
        mBottomSheetDialog = BottomSheetDialog(this).apply {
            setContentView(layoutInflater.inflate(R.layout.layout_stickers, null)
                    .apply { findViewById<RecyclerView>(R.id.rv_stickers).adapter = mStickerAdapter })
            setCancelable(true)
            setCanceledOnTouchOutside(true)
        }

        findViewById<ImageButton>(R.id.btn_change_text_style).setOnClickListener {
            when (mEditorViewLayout.style) {
                STYLE_SIMPLE -> mEditorViewLayout.style = STYLE_WRAP_TEXT
                STYLE_WRAP_TEXT -> mEditorViewLayout.style = STYLE_WRAP_TEXT_WITH_ALPHA
                else -> mEditorViewLayout.style = STYLE_SIMPLE
            }
        }
        findViewById<ImageButton>(R.id.btn_add_sticker).setOnClickListener { mBottomSheetDialog.show() }
        findViewById<Button>(R.id.btn_save).setOnClickListener { onSaveImage() }

        mPresenter.attachView(this)
        mPresenter.startLoading {
            if (savedInstanceState != null) {
                mEditorViewLayout.restoreComplicatedSavedState(mStateHolder.getState(SAVED_STATE_EDITOR_VIEW))
                mEditorViewLayout.style = savedInstanceState.getInt(SAVED_STATE_TEXT_STYLE_POSITION)
                if (!savedInstanceState.getBoolean(SAVED_STATE_USER_SELECTS_PICTURE)) {
                    if (savedInstanceState.containsKey(SAVED_STATE_USER_IMAGE)) {
                        restoreUserImage(savedInstanceState.getParcelable(SAVED_STATE_USER_IMAGE) as Uri)
                    } else {
                        pickBgPosition(savedInstanceState.getInt(SAVED_STATE_BG_POSITION))
                    }
                }
            } else {
                pickBgPosition(FIRST_BG_POSITION)
            }
        }
    }

    private fun onSaveImage() = ActivityCompat.requestPermissions(
            this,
            arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
            PERMISSION_REQUEST_CODE_FOR_SAVE
    )

    override fun callGallery() {
        mUserIsSelectingPic = true
        startActivityForResult(
                Intent(Intent.ACTION_PICK).apply { type = "image/*" }, GET_IMAGE_REQUEST_CODE
        )
    }

    override fun setImage(background: Bitmap, top: Bitmap?, bottom: Bitmap?, scaleType: Int) {
        mEditorViewLayout.setBackgroundImage(background, top, bottom, scaleType)
        mEditorViewLayout.tag = null
    }

    override fun addSticker(stickerBitmap: Bitmap) = mEditorViewLayout.addSticker(stickerBitmap)

    override fun hideStickerMenu() = mBottomSheetDialog.dismiss()

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            GET_IMAGE_REQUEST_CODE -> {
                mUserIsSelectingPic = false
                if (resultCode == RESULT_OK && data != null) {
                    if (mEditorViewLayout.measuredWidth == 0 && mEditorViewLayout.measuredHeight == 0) {
                        // Activity been killed, while user select a pic
                        restoreUserImage(data.data)
                    } else {
                        setUserImageToView(data.data)
                    }
                }
            }
        }
    }

    private fun setUserImageToView(uri: Uri) {
        val selectedImage = mImageLoader.getUserImage(uri, mEditorViewLayout.measuredWidth,
                mEditorViewLayout.measuredHeight)
        if (selectedImage != null) {
            mEditorViewLayout.tag = uri
            mEditorViewLayout.setBackgroundImage(selectedImage, null, null, EditorView.ScaleType.NONE)
            mBgAdapter.deselectItem()
        } else {
            Toast.makeText(this, "Unable to receive image", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            PERMISSION_REQUEST_CODE_FOR_SAVE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mPresenter.onSave(mEditorViewLayout.getFinalImage())
                } else {
                    Toast.makeText(
                            this@MainActivity,
                            "Permission denied to write your storage",
                            Toast.LENGTH_SHORT
                    ).show()
                }
                return
            }
        }
    }

    private fun pickBgPosition(position: Int) = mEditorViewLayout.viewTreeObserver
            .addOnPreDrawListener(object :
                    ViewTreeObserver.OnPreDrawListener {
                override fun onPreDraw(): Boolean {
                    mEditorViewLayout.viewTreeObserver.removeOnPreDrawListener(this)
                    mBgAdapter.pickPosition(position)
                    return true
                }
            })

    private fun restoreUserImage(uri: Uri) = mEditorViewLayout.viewTreeObserver
            .addOnPreDrawListener(object :
                    ViewTreeObserver.OnPreDrawListener {
                override fun onPreDraw(): Boolean {
                    mEditorViewLayout.viewTreeObserver.removeOnPreDrawListener(this)
                    setUserImageToView(uri)
                    return true
                }
            })

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        mStateHolder.saveState(SAVED_STATE_EDITOR_VIEW, mEditorViewLayout.getComplicatedSavedState())
        outState?.putBoolean(SAVED_STATE_USER_SELECTS_PICTURE, mUserIsSelectingPic)
        if (mEditorViewLayout.tag != null) {
            outState?.putParcelable(SAVED_STATE_USER_IMAGE, mEditorViewLayout.tag as Uri)
        } else {
            outState?.putInt(SAVED_STATE_BG_POSITION, mBgAdapter.checkedPosition)
        }
        outState?.putInt(SAVED_STATE_TEXT_STYLE_POSITION, mEditorViewLayout.style)
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter.destroy()
    }

    override fun showOkSaving() = Toast.makeText(this, "Success", Toast.LENGTH_SHORT).show()

    companion object {
        private const val GET_IMAGE_REQUEST_CODE = 111
        private const val PERMISSION_REQUEST_CODE_FOR_SAVE = 1

        private const val FIRST_BG_POSITION = 0

        private const val SAVED_STATE_BG_POSITION = "SAVED_STATE_BG_POSITION"
        private const val SAVED_STATE_TEXT_STYLE_POSITION = "SAVED_STATE_TEXT_STYLE_POSITION"
        private const val SAVED_STATE_EDITOR_VIEW = "SAVED_STATE_EDITOR_VIEW"
        private const val SAVED_STATE_USER_IMAGE = "SAVED_STATE_USER_IMAGE"
        private const val SAVED_STATE_USER_SELECTS_PICTURE = "SAVED_STATE_USER_SELECTS_PICTURE"
    }
}
