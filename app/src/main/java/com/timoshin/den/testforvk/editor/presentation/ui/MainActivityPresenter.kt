package com.timoshin.den.testforvk.editor.presentation.ui

import android.graphics.Bitmap
import com.timoshin.den.testforvk.editor.domain.entity.BackgroundItem
import com.timoshin.den.testforvk.editor.domain.entity.ImageResult
import com.timoshin.den.testforvk.editor.domain.entity.StickerItem
import com.timoshin.den.testforvk.editor.domain.interactors.EditorInteractor
import com.timoshin.den.testforvk.editor.presentation.base.BasePresenter
import com.timoshin.den.testforvk.editor.presentation.base.MvpAdapter
import com.timoshin.den.testforvk.injection.mainactivity.PerMainActivity
import javax.inject.Inject

@PerMainActivity
class MainActivityPresenter @Inject constructor(
        private val mBackgroundsAdapter: MvpAdapter<BackgroundItem>,
        private val mStickerAdapter: MvpAdapter<StickerItem>,
        private val mInteractor: EditorInteractor
) : BasePresenter<MainActivityContract.View>(), MainActivityContract.Presenter {

    override fun attachView(mvpView: MainActivityContract.View) {
        super.attachView(mvpView)
        mBackgroundsAdapter.attachPresenter(this)
        mStickerAdapter.attachPresenter(this)
    }

    override fun startLoading(onFinishLoad: () -> Unit) {
        addDisposable(
                mInteractor.getResources()
                        .subscribe({
                            mBackgroundsAdapter.addList(it.first)
                            mStickerAdapter.addList(it.second)
                            onFinishLoad()
                        }, { it.printStackTrace() })
        )
    }

    override fun addNewImage() {
        view?.callGallery()
    }

    override fun onStickerSelected(id: Int, stickerBitmap: Bitmap) {
        view?.addSticker(stickerBitmap)
        view?.hideStickerMenu()
    }

    override fun addDefaultImage(
            background: Bitmap,
            top: Bitmap?,
            bottom: Bitmap?,
            scaleType: Int
    ) {
        view?.setImage(background, top, bottom, scaleType)
    }

    override fun onSave(finalImage: Bitmap) {
        addDisposable(mInteractor.saveImage(ImageResult(finalImage))
                .subscribe({
                    view?.showOkSaving()
                }, { it.printStackTrace() })
        )
    }
}