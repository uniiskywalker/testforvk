package com.timoshin.den.testforvk.editor.presentation.ui.editor

import android.graphics.Canvas
import android.graphics.Paint
import android.text.style.LineBackgroundSpan

class PaddingBackgroundColorSpan(private val mHandler: (lineNumber: Int,
                                                        start: Int,
                                                        end: Int,
                                                        top: Int,
                                                        bottom: Int) -> Unit) :
        LineBackgroundSpan {

    override fun drawBackground(c: Canvas,
                                p: Paint,
                                left: Int,
                                right: Int,
                                top: Int,
                                baseline: Int,
                                bottom: Int,
                                text: CharSequence,
                                start: Int,
                                end: Int,
                                lnum: Int) {
        val textWidth = Math.round(p.measureText(text, start, end))
        val centerX = right / 2
        val textWidthCenter = textWidth / 2
        val startX = centerX - textWidthCenter
        val endX = centerX + textWidthCenter
        mHandler(lnum, startX, endX, top, bottom)
    }
}