package com.timoshin.den.testforvk.editor.data

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory

abstract class FromAssetsSource(val context: Context) {

    fun getAsset(path: String): Bitmap = BitmapFactory.decodeStream(context.assets.open(path))
}