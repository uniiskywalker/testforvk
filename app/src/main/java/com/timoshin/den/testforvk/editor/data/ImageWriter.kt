package com.timoshin.den.testforvk.editor.data

import android.content.Context
import android.provider.MediaStore
import com.timoshin.den.testforvk.editor.domain.entity.ImageResult
import com.timoshin.den.testforvk.editor.domain.interfaces.Writer
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class ImageWriter(val context: Context) : Writer<ImageResult> {
    override fun create(value: ImageResult): Completable = Completable.create {
        try {
            MediaStore.Images.Media.insertImage(
                    context.contentResolver,
                    value.bitmap,
                    "vktest-${System.currentTimeMillis()}.png",
                    "Vk test image"
            )
            it.onComplete()
        } catch (e: Exception) {
            it.onError(e)
        }
    }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
}