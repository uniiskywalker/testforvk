package com.timoshin.den.testforvk.editor.domain.entity

import android.graphics.Bitmap

data class StickerItem(val id: Int, val stickerBitmap: Bitmap)
