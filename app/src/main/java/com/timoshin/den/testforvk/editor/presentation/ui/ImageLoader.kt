package com.timoshin.den.testforvk.editor.presentation.ui

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.net.Uri
import android.support.media.ExifInterface
import java.io.IOException


class ImageLoader constructor(private val context: Context) {

    fun getUserImage(uri: Uri, targetWidth: Int, targetHeight: Int): Bitmap? {
        try {
            return setProperOrientation(uri, BitmapFactory.Options().run {
                inJustDecodeBounds = true
                BitmapFactory.decodeStream(context.contentResolver.openInputStream(uri), null, this)
                inSampleSize = calculateInSampleSize(
                        this,
                        targetWidth,
                        targetHeight
                )
                inJustDecodeBounds = false
                BitmapFactory.decodeStream(context.contentResolver.openInputStream(uri), null, this)
            })
        } catch (e: IOException) {
            e.printStackTrace()
            return null
        }
    }

    private fun calculateInSampleSize(options: BitmapFactory.Options, reqWidth: Int, reqHeight: Int): Int {
        val (height: Int, width: Int) = options.run { outHeight to outWidth }
        var inSampleSize = 1

        if (height > reqHeight || width > reqWidth) {
            val halfHeight: Int = height / 2
            val halfWidth: Int = width / 2
            while (halfHeight / inSampleSize >= reqHeight && halfWidth / inSampleSize >= reqWidth) {
                inSampleSize *= 2
            }
        }
        return inSampleSize
    }

    private fun setProperOrientation(uri: Uri, bitmap: Bitmap): Bitmap {
        val exifInterface = ExifInterface(context.contentResolver.openInputStream(uri))
        val orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED)
        val matrix = Matrix()
        when (orientation) {
            ExifInterface.ORIENTATION_ROTATE_90 -> matrix.setRotate(90f)
            ExifInterface.ORIENTATION_ROTATE_180 -> matrix.setRotate(180f)
        }
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
    }
}