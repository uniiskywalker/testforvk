package com.timoshin.den.testforvk.editor.presentation.ui.editor

import android.view.MotionEvent

class RotationGestureDetector(
        private val isRelativeAngleMode: Boolean,
        private val mListener: (RotationGestureDetector) -> Unit) {

    companion object {
        private const val INVALID_ID = -1
    }

    private var pointer1: Int = INVALID_ID
    private var pointer2: Int = INVALID_ID

    private var line1StartX: Float = 0f
    private var line1StartY: Float = 0f
    private var line1EndX: Float = 0f
    private var line1EndY: Float = 0f

    var angle: Float = 0f
        private set

    var intersectionPoint: Pair<Float, Float>? = null
        private set

    var focalPoint: Pair<Float, Float>? = null
        private set

    fun onTouchEvent(event: MotionEvent): Boolean {
        when (event.actionMasked) {
            MotionEvent.ACTION_DOWN -> {
                pointer1 = event.getPointerId(event.actionIndex)
            }
            MotionEvent.ACTION_POINTER_DOWN -> {
                pointer2 = event.getPointerId(event.actionIndex)
                line1StartX = event.getX(event.findPointerIndex(pointer2))
                line1StartY = event.getY(event.findPointerIndex(pointer2))
                line1EndX = event.getX(event.findPointerIndex(pointer1))
                line1EndY = event.getY(event.findPointerIndex(pointer1))
            }
            MotionEvent.ACTION_MOVE -> if (pointer1 != INVALID_ID && pointer2 != INVALID_ID) {
                val line2StartX: Float = event.getX(event.findPointerIndex(pointer2))
                val line2StartY: Float = event.getY(event.findPointerIndex(pointer2))
                val line2EndX: Float = event.getX(event.findPointerIndex(pointer1))
                val line2EndY: Float = event.getY(event.findPointerIndex(pointer1))

                angle = angleBetweenLines(
                        line1StartX, line1StartY, line1EndX, line1EndY,
                        line2StartX, line2StartY, line2EndX, line2EndY
                )
                /*     intersectionPoint = getIntersectionPoint(
                             line1StartX, line1StartY, line1EndX,
                             line1EndY, line2StartX, line2StartY, line2EndX, line2EndY
                     )*/
                focalPoint = getFocalPoint(line1StartX, line1StartY, line1EndX, line1EndY)
                isRelativeAngleMode.takeIf { it }?.apply {
                    line1StartX = line2StartX
                    line1StartY = line2StartY
                    line1EndX = line2EndX
                    line1EndY = line2EndY
                }
                mListener(this)
            }
            MotionEvent.ACTION_UP -> pointer1 = INVALID_ID
            MotionEvent.ACTION_POINTER_UP -> pointer2 = INVALID_ID
            MotionEvent.ACTION_CANCEL -> {
                pointer1 = INVALID_ID
                pointer2 = INVALID_ID
            }
        }
        return true
    }

    private fun angleBetweenLines(line1StartX: Float,
                                  line1StartY: Float,
                                  line1EndX: Float,
                                  line1EndY: Float,
                                  line2StartX: Float,
                                  line2StartY: Float,
                                  line2EndX: Float,
                                  line2EndY: Float): Float {
        val angle1 = Math.atan2((line1StartY - line1EndY).toDouble(), (line1StartX - line1EndX).toDouble()).toFloat()
        val angle2 = Math.atan2((line2StartY - line2EndY).toDouble(), (line2StartX - line2EndX).toDouble()).toFloat()

        var angle = Math.toDegrees((angle1 - angle2).toDouble()).toFloat() % 360
        if (angle < -180f) angle += 360.0f
        if (angle > 180f) angle -= 360.0f
        return -1 * angle
    }

    private fun getIntersectionPoint(line1StartX: Float,
                                     line1StartY: Float,
                                     line1EndX: Float,
                                     line1EndY: Float,
                                     line2StartX: Float,
                                     line2StartY: Float,
                                     line2EndX: Float,
                                     line2EndY: Float): Pair<Float, Float>? {

        val a1 = line1EndY - line1StartY
        val b1 = line1StartX - line1EndX
        val c1 = a1 * line1StartX + b1 * line1StartY

        val a2 = line2EndY - line2StartY
        val b2 = line2StartX - line2EndX
        val c2 = a2 * line2StartX + b2 * line2StartY

        val delta = a1 * b2 - a2 * b1
        return (b2 * c1 - b1 * c2) / delta to (a1 * c2 - a2 * c1) / delta
    }

    private fun getFocalPoint(line1StartX: Float,
                              line1StartY: Float,
                              line1EndX: Float,
                              line1EndY: Float): Pair<Float, Float>? {
        return (line1StartX + line1EndX) / 2 to (line1StartY + line1EndY) / 2
    }
}