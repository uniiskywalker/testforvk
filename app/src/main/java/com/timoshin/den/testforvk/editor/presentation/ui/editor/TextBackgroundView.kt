package com.timoshin.den.testforvk.editor.presentation.ui.editor

import android.content.Context
import android.graphics.Canvas
import android.graphics.CornerPathEffect
import android.graphics.Paint
import android.graphics.Path
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import com.timoshin.den.testforvk.util.convertDpToPixel

class TextBackgroundView : View {

    private val mLines = HashMap<Int, Line>()
    private var mPaint = Paint().apply {
        isAntiAlias = true
        strokeWidth = 6f
        pathEffect = CornerPathEffect(20f)
    }
    private val mPath = Path()
    private val linePadding = convertDpToPixel(8f).toInt()

    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        return false
    }

    fun addLine(lineNumber: Int, start: Int, end: Int, top: Int, bottom: Int) {
        mLines[lineNumber] = (Line(lineNumber,
                start + paddingStart - linePadding,
                end + paddingStart + linePadding,
                top + paddingTop,
                bottom + paddingStart))
        invalidate()
    }

    fun setColor(color: Int) {
        mPaint.color = color
    }

    fun clearLines() {
        mLines.clear()
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        if (mLines.size == 0) {
            return
        }
        canvas?.apply {
            mPath.reset()

            var line = mLines[0]
            if (line != null) {
                mPath.moveTo(line.start.toFloat(), line.top.toFloat())
                mPath.lineTo(line.end.toFloat(), line.top.toFloat())
                mPath.lineTo(line.end.toFloat(), line.bottom.toFloat())
            }

            for (i in 1..(mLines.size - 2)) {
                line = mLines[i]
                if (line != null) {
                    mPath.lineTo(line.end.toFloat(), line.top.toFloat())
                    mPath.lineTo(line.end.toFloat(), line.bottom.toFloat())
                }
            }

            line = mLines[mLines.size - 1]
            if (line != null) {
                mPath.lineTo(line.end.toFloat(), line.top.toFloat())
                mPath.lineTo(line.end.toFloat(), line.bottom.toFloat() + linePadding / 2)
                mPath.lineTo(line.start.toFloat(), line.bottom.toFloat() + linePadding / 2)
                mPath.lineTo(line.start.toFloat(), line.top.toFloat())
            }

            for (i in (mLines.size - 2) downTo 1) {
                line = mLines[i]
                if (line != null) {
                    mPath.lineTo(line.start.toFloat(), line.bottom.toFloat())
                    mPath.lineTo(line.start.toFloat(), line.top.toFloat())
                }
            }

            line = mLines[0]
            if (line != null) {
                mPath.lineTo(line.start.toFloat(), line.bottom.toFloat())
                mPath.lineTo(line.start.toFloat(), line.top.toFloat())
            }
            mPath.close()
            drawPath(mPath, mPaint)
        }
    }

    data class Line(val lineNumber: Int, val start: Int, val end: Int, val top: Int, val bottom: Int)
}