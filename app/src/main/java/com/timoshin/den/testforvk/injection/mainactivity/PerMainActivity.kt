package com.timoshin.den.testforvk.injection.mainactivity

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class PerMainActivity