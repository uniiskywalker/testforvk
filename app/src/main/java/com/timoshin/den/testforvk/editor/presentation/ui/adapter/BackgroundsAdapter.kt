package com.timoshin.den.testforvk.editor.presentation.ui.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import com.timoshin.den.testforvk.R
import com.timoshin.den.testforvk.editor.domain.entity.BackgroundItem
import com.timoshin.den.testforvk.editor.presentation.base.MvpAdapter
import com.timoshin.den.testforvk.editor.presentation.base.MvpPresenter
import com.timoshin.den.testforvk.editor.presentation.base.MvpView
import com.timoshin.den.testforvk.editor.presentation.ui.MainActivityContract
import java.util.*

class BackgroundsAdapter(val context: Context) :
        RecyclerView.Adapter<RecyclerView.ViewHolder>(),
        MvpAdapter<BackgroundItem> {

    companion object {
        private const val TYPE_ITEM = 0
        private const val TYPE_FOOTER = 1
        private const val DESELECTED = -1
    }

    private lateinit var mPresenter: MainActivityContract.Presenter
    private var mBackgroundItems: List<BackgroundItem> = Collections.emptyList()
    var checkedPosition = 0
        private set

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return viewType.takeIf { it == TYPE_FOOTER }?.let {
            return@let FooterViewHolder(inflate(parent, R.layout.footer_item))
        } ?: ItemViewHolder(inflate(parent, R.layout.background_item) as ImageButton)
    }

    private fun inflate(parent: ViewGroup, res: Int) = LayoutInflater.from(parent.context).inflate(res, parent, false)

    override fun getItemCount(): Int {
        return mBackgroundItems.size + 1
    }

    override fun getItemViewType(position: Int): Int {
        if (position == mBackgroundItems.size) {
            return TYPE_FOOTER
        }
        return TYPE_ITEM
    }

    override fun onBindViewHolder(holderItem: RecyclerView.ViewHolder, position: Int) {
        holderItem.takeIf { it is ItemViewHolder }?.let { it as ItemViewHolder }?.apply {
            ib.background = mBackgroundItems[position].thumb
            ib.isActivated = position == checkedPosition
        }
    }

    fun pickPosition(position: Int) {
        selectPosition(position)
        mPresenter.addDefaultImage(
                mBackgroundItems[position].background,
                mBackgroundItems[position].top,
                mBackgroundItems[position].bottom,
                mBackgroundItems[position].scale
        )
    }

    private fun selectPosition(position: Int) {
        if (position > itemCount) return
        val lastPosition = checkedPosition
        checkedPosition = position
        notifyItemChanged(lastPosition)
        notifyItemChanged(checkedPosition)
    }

    inner class ItemViewHolder(val ib: ImageButton) : RecyclerView.ViewHolder(ib) {
        init {
            ib.setOnClickListener {
                pickPosition(adapterPosition)
            }
        }
    }

    inner class FooterViewHolder(ib: View) : RecyclerView.ViewHolder(ib) {
        init {
            ib.setOnClickListener {
                mPresenter.addNewImage()
            }
        }
    }

    override fun <P : MvpPresenter<out MvpView>> attachPresenter(presenter: P) {
        mPresenter = presenter as MainActivityContract.Presenter
    }

    override fun addList(item: List<BackgroundItem>) {
        mBackgroundItems = item
        notifyDataSetChanged()
    }

    override fun get(position: Int): BackgroundItem = mBackgroundItems[position]

    fun deselectItem() {
        val lastPosition = checkedPosition
        checkedPosition = DESELECTED
        notifyItemChanged(lastPosition)
    }
}