package com.timoshin.den.testforvk.editor.presentation.ui.editor

import android.content.Context
import android.view.MotionEvent
import android.view.ScaleGestureDetector

class GestureHandler<T>(
        context: Context,
        private val objectDetector: (targetX: Float, targetY: Float) -> T?,
        private val onTranslate: (obj: T, prevX: Float, prevY: Float, newX: Float, newY: Float) -> Unit,
        private val onStopTranslate: (obj: T) -> Unit,
        private val onScale: (obj: T, scaleDiff: Float) -> Unit,
        private val onRotate: (obj: T, angle: Float, relativePointX: Float, relativePointY: Float) -> Unit
) {

    private var objBeingChanged: T? = null
    private var lastMovingX = 0f
    private var lastMovingY = 0f
    private val mScaleDetector = ScaleGestureDetector(context, object : ScaleGestureDetector.SimpleOnScaleGestureListener() {

        override fun onScale(detector: ScaleGestureDetector): Boolean {
            objectDetector(detector.focusX, detector.focusY)
                    ?.apply { onScale(this, detector.scaleFactor) }
            return true
        }
    })
    private val mRotationDetector = RotationGestureDetector(true) {
        it.focalPoint?.apply {
            objectDetector(first, second)?.apply { onRotate(this, it.angle, first, second) }
        }
    }

    fun handleTouchEvent(event: MotionEvent): Boolean {
        mRotationDetector.onTouchEvent(event)
        if (event.pointerCount > 1) {
            clearTranslationGarbage()
            mScaleDetector.onTouchEvent(event)
            return true
        }
        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                objBeingChanged = objectDetector(event.x, event.y)
                lastMovingX = event.x
                lastMovingY = event.y
            }
            MotionEvent.ACTION_UP, MotionEvent.ACTION_CANCEL -> {
                clearTranslationGarbage()
            }
            MotionEvent.ACTION_MOVE -> {
                objBeingChanged?.apply { onTranslate(this, lastMovingX, lastMovingY, event.x, event.y) }
                lastMovingX = event.x
                lastMovingY = event.y
            }
        }
        return true
    }

    private fun clearTranslationGarbage() {
        objBeingChanged?.apply { onStopTranslate(this) }
        objBeingChanged = null
        lastMovingX = 0f
        lastMovingY = 0f
    }
}